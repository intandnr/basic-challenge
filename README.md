# Basic Challenge

Try to do this to learn new language

## 01. generate random number
create a function that return random number between 0-100

## 02. generate random string
create a function that return random string that could contain lower-case, upper-case, numeric, special character

## 03. string and array
create a function that split string into array
> input: "udin, ujang, asep"
> output: ["udin", "ujang", "asep"]

## 04. detect leap year
create a function that return true if year is a leap year and false if it was not a leap year

## 05. print object
create a function that print data from array of object
input:
```
[
    {
        name:"udin",
        age:10
    },
    {
        name:"ujang",
        age:11
    },
    {
        name:"asep",
        age:12
    }
]
```

output:
"1. nama: udin, usia: 10"
"2. nama: ujang, usia: 11"
"3. nama: asep, usia: 12"

## 06. classification
create a function that return object that contain gruop of people based on sex, age, marital-status, job-status
input:
```
[
    {
        name:"udin",
        sex:"male",
        age:10,
        marital:"single",
        social:"student"
    },
    {
        name:"ujang",
        sex:"male",
        age:25,
        marital:"married",
        social:"employee"
    },
    {
        name:"icih",
        sex:"female",
        age:10,
        marital:"single",
        social:"student"
    },
    {
        name:"eneng",
        sex:"female",
        age:100,
        marital:"married",
        social:"employee"
    },
    {
        name:"asep",
        sex:"male",
        age:20,
        marital:"single",
        social:"employee"
    },
]
```
output:
```
{
    sex: {
        male:["udin", "ujang", "asep"], 
        female:["icih", "eneng"]
        },
    age: {
        under20: ["udin", "icih"],
        older:["asep", "eneng", "ujang"]
    },
    marriage: {
        single: ["asep", "icih", "udin"],
        double: ["eneng", "ujang"]
    },
    status: {
        student: ["icih", "udin"],
        employee: ["eneng", "asep", "ujang"]
    }
}
```
## 07. find A
create a function that count number of a from given lopped word
input:
```
word: aha
no of loop: 3
```

output:
```
6
reason:
aha * 3 = ahaahaaha
therefore 6 a is present
```
